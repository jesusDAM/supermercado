<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TelefonoProveedores */

$this->title = 'Create Telefono Proveedores';
$this->params['breadcrumbs'][] = ['label' => 'Telefono Proveedores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="telefono-proveedores-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
