<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TelefonoProveedores */

$this->title = 'Update Telefono Proveedores: ' . $model->id_proveedor;
$this->params['breadcrumbs'][] = ['label' => 'Telefono Proveedores', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_proveedor, 'url' => ['view', 'id' => $model->id_proveedor]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="telefono-proveedores-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
