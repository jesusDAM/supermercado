<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Home', 'url' => ['/site/index']],
            ['label' => 'Empleados', 'url' => ['/empleados/index']],
            ['label' => 'DireccionEmpleados', 'url' => ['/direccion-empleados/index']],
            ['label' => 'TelefonoEmpleados', 'url' => ['/telefono-empleados/index']],
            ['label' => 'Cargos', 'url' => ['/cargos/index']],
            ['label' => 'Ventas', 'url' => ['/ventas/index']],
            ['label' => 'Compras', 'url' => ['/compras/index']],
            ['label' => 'Productos', 'url' => ['/productos/index']],
            ['label' => 'ProductosCompras', 'url' => ['/productos-compras/index']],
            ['label' => 'ProductosVentas', 'url' => ['/productos-ventas/index']],
            ['label' => 'ProductosMarcas', 'url' => ['/productos-marcas/index']],
            ['label' => 'Marcas', 'url' => ['/marcas/index']],
            ['label' => 'Proveedores', 'url' => ['/proveedores/index']],
            ['label' => 'DireccionProveedores', 'url' => ['/dirreccion-proveedores/index']],
            ['label' => 'TelefonoProveedores', 'url' => ['/telefono-proveedores/index']],
            ['label' => 'Departamentos', 'url' => ['/departamentos/index']],
            ['label' => 'About', 'url' => ['/site/about']],
            ['label' => 'Contact', 'url' => ['/site/contact']],
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
