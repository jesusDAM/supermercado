<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProductosMarcas */

$this->title = 'Update Productos Marcas: ' . $model->id_marca;
$this->params['breadcrumbs'][] = ['label' => 'Productos Marcas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_marca, 'url' => ['view', 'id' => $model->id_marca]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="productos-marcas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
