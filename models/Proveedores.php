<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "proveedores".
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $apellidos
 *
 * @property DireccionProveedores $direccionProveedores
 * @property Marcas[] $marcas
 * @property TelefonoProveedores $telefonoProveedores
 */
class Proveedores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'proveedores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'string', 'max' => 20],
            [['apellidos'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
        ];
    }

    /**
     * Gets query for [[DireccionProveedores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDireccionProveedores()
    {
        return $this->hasOne(DireccionProveedores::className(), ['id_proveedor' => 'id']);
    }

    /**
     * Gets query for [[Marcas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMarcas()
    {
        return $this->hasMany(Marcas::className(), ['id_proveedor' => 'id']);
    }

    /**
     * Gets query for [[TelefonoProveedores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTelefonoProveedores()
    {
        return $this->hasOne(TelefonoProveedores::className(), ['id_proveedor' => 'id']);
    }
}
