<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ventas".
 *
 * @property int $id
 * @property string|null $fecha
 * @property float|null $precio
 * @property int|null $descuento
 * @property int|null $id_empleado
 *
 * @property ProductosVentas[] $productosVentas
 * @property Empleados $empleado
 */
class Ventas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ventas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha'], 'safe'],
            [['precio'], 'number'],
            [['descuento', 'id_empleado'], 'integer'],
            [['id_empleado'], 'exist', 'skipOnError' => true, 'targetClass' => Empleados::className(), 'targetAttribute' => ['id_empleado' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fecha' => 'Fecha',
            'precio' => 'Precio',
            'descuento' => 'Descuento',
            'id_empleado' => 'Id Empleado',
        ];
    }

    /**
     * Gets query for [[ProductosVentas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductosVentas()
    {
        return $this->hasMany(ProductosVentas::className(), ['id_venta' => 'id']);
    }

    /**
     * Gets query for [[Empleado]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmpleado()
    {
        return $this->hasOne(Empleados::className(), ['id' => 'id_empleado']);
    }
}
