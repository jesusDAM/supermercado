<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "productos_ventas".
 *
 * @property int|null $id_producto
 * @property int|null $id_venta
 * @property int|null $cantidad
 * @property float|null $precio_unidad
 *
 * @property Productos $producto
 * @property Ventas $venta
 */
class ProductosVentas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'productos_ventas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_producto', 'id_venta', 'cantidad'], 'integer'],
            [['precio_unidad'], 'number'],
            [['id_producto'], 'exist', 'skipOnError' => true, 'targetClass' => Productos::className(), 'targetAttribute' => ['id_producto' => 'id']],
            [['id_venta'], 'exist', 'skipOnError' => true, 'targetClass' => Ventas::className(), 'targetAttribute' => ['id_venta' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_producto' => 'Id Producto',
            'id_venta' => 'Id Venta',
            'cantidad' => 'Cantidad',
            'precio_unidad' => 'Precio Unidad',
        ];
    }

    /**
     * Gets query for [[Producto]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProducto()
    {
        return $this->hasOne(Productos::className(), ['id' => 'id_producto']);
    }

    /**
     * Gets query for [[Venta]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVenta()
    {
        return $this->hasOne(Ventas::className(), ['id' => 'id_venta']);
    }
}
