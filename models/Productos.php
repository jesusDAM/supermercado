<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "productos".
 *
 * @property int $id
 * @property string|null $nombre
 * @property int|null $unidades
 * @property float|null $precio
 * @property int|null $id_departamento
 *
 * @property Departamentos $departamento
 * @property ProductosCompras[] $productosCompras
 * @property ProductosMarcas[] $productosMarcas
 * @property ProductosVentas[] $productosVentas
 */
class Productos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'productos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['unidades', 'id_departamento'], 'integer'],
            [['precio'], 'number'],
            [['nombre'], 'string', 'max' => 50],
            [['id_departamento'], 'exist', 'skipOnError' => true, 'targetClass' => Departamentos::className(), 'targetAttribute' => ['id_departamento' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'unidades' => 'Unidades',
            'precio' => 'Precio',
            'id_departamento' => 'Id Departamento',
        ];
    }

    /**
     * Gets query for [[Departamento]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDepartamento()
    {
        return $this->hasOne(Departamentos::className(), ['id' => 'id_departamento']);
    }

    /**
     * Gets query for [[ProductosCompras]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductosCompras()
    {
        return $this->hasMany(ProductosCompras::className(), ['id_producto' => 'id']);
    }

    /**
     * Gets query for [[ProductosMarcas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductosMarcas()
    {
        return $this->hasMany(ProductosMarcas::className(), ['id_producto' => 'id']);
    }

    /**
     * Gets query for [[ProductosVentas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductosVentas()
    {
        return $this->hasMany(ProductosVentas::className(), ['id_producto' => 'id']);
    }
}
